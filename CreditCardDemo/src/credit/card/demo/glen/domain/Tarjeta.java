package credit.card.demo.glen.domain;


public class Tarjeta {
	private Marca marca;
	private String nroTarjeta;
    private String cardHolder ;   
    private String fechaVto;   //formato dd/MM/yyyy  ejemplo 30/06/2021
    private double monto;
	
    public Tarjeta() {
		super();
	}
    
    public Tarjeta(Marca marca, String nroTarjeta, String cardHolder, String fechaVto, double monto) {
		super();
		this.marca = marca;
		this.nroTarjeta = nroTarjeta;
		this.cardHolder = cardHolder;
		this.fechaVto = fechaVto;
		this.monto = monto;
	}
    
   
    
    
    
    
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	public String getNroTarjeta() {
		return nroTarjeta;
	}
	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
	public String getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	public String getFechaVto() {
		return fechaVto;
	}
	public void setFechaVto(String fechaVto) {
		this.fechaVto = fechaVto;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardHolder == null) ? 0 : cardHolder.hashCode());
		result = prime * result + ((fechaVto == null) ? 0 : fechaVto.hashCode());
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((nroTarjeta == null) ? 0 : nroTarjeta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarjeta other = (Tarjeta) obj;
		if (cardHolder == null) {
			if (other.cardHolder != null)
				return false;
		} else if (!cardHolder.equals(other.cardHolder))
			return false;
		if (fechaVto == null) {
			if (other.fechaVto != null)
				return false;
		} else if (!fechaVto.equals(other.fechaVto))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (nroTarjeta == null) {
			if (other.nroTarjeta != null)
				return false;
		} else if (!nroTarjeta.equals(other.nroTarjeta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Marca: "+marca.getNombreMarca() +
				", Nro Tarjeta: " +nroTarjeta+",Titular: "
				+cardHolder+", Vigencia: "+fechaVto+", Monto: "+monto ;
	}
    
}
