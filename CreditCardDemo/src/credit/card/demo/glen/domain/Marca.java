package credit.card.demo.glen.domain;

import credit.card.demo.glen.tasa.Tasa;

public class Marca {
	
	private String nombreMarca;
    private Tasa tasa;

	
	public Marca() {
		super();
	}
    
	public Marca(String nombreMarca, Tasa tasa) {
		super();
		this.nombreMarca = nombreMarca;
		this.tasa = tasa;
	}
	


	public Tasa getTasa() {
		return tasa;
	}

	public void setTasa(Tasa tasa) {
		this.tasa = tasa;
	}

	public String getNombreMarca() {
		return nombreMarca;
	}

	public void setNombreMarca(String nombreMarca) {
		this.nombreMarca = nombreMarca;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombreMarca == null) ? 0 : nombreMarca.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marca other = (Marca) obj;
		if (nombreMarca == null) {
			if (other.nombreMarca != null)
				return false;
		} else if (!nombreMarca.equals(other.nombreMarca))
			return false;
		return true;
	}

    
    
}
