package credit.card.demo.glen.excepcion;


public class ManejadorExcepciones {
	
	private ManejadorExcepciones() {

	}
	
	
	
	public static HandledException manejarException(Throwable e) {
		
		if (e instanceof HandledException) {
			return (HandledException) (e);
		}else if (e instanceof MontoActualizableException) {
			return (MontoActualizableException((HandledException) e));
		}else if (e instanceof PrintException) {
			return (ManejadorExcepciones.PrintException((HandledException) e));
		}else if (e instanceof HostException) {
			return (ManejadorExcepciones.HostException((HandledException) e));
		}else if (e != null) {
			return (new NotHandledException("ERROR NO CONTROLADO!!!", e));
		} else {
			return (new NotHandledException("ERROR FATAL!!!",null));
		}
	}

	
	
	
	private static HandledException HostException(HandledException e) {
		// TODO Auto-generated method stub
		return (HandledException) e;
	}

	private static HandledException PrintException(HandledException e) {
		// TODO Auto-generated method stub
		return (HandledException) e;
	}

	private static HandledException MontoActualizableException(HandledException e) {
		
		System.out.println("Exception Controlada del tipo MontoActualizableException:");
		System.out.println("Mensaje: "+e.getMessage());
		
		return (HandledException) e;
	}
}
