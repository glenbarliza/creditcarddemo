package credit.card.demo.glen.excepcion;

public class HandledException extends Exception{
	
	
	public HandledException() {
		super();
	}
	
	public HandledException(Throwable ex) {
		super();
	}
	
	public HandledException(String mensaje, Throwable ex) {
		super(mensaje, ex);
	}

}
