package credit.card.demo.glen.excepcion;


public class MontoActualizableException extends HandledException{

	public MontoActualizableException(String msg, Exception e) {
		
		super(msg, e.getCause());
	}

}
