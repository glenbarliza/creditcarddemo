package credit.card.demo.glen.excepcion;

public class NotHandledException extends HandledException{
	
	public NotHandledException() {
		super();
	}
	
	public NotHandledException(Throwable arg1) {
		super();
	}
	
	public NotHandledException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
