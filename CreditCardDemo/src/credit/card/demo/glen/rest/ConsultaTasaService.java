package credit.card.demo.glen.rest;
 

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import credit.card.demo.glen.tasa.PereTasaImpl;
import credit.card.demo.glen.tasa.ScoTasaImpl;
import credit.card.demo.glen.tasa.SquaTasaImpl;
import credit.card.demo.glen.tasa.Tasa;

 
@Path("/tasa")
public class ConsultaTasaService {
	
	private HashMap<String,Tasa> tasaSegunMarca;
	
	
	@GET
	@Path("{marca}/{monto}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultaTasa(@PathParam("marca") String marca, @PathParam("monto") Double monto) {	
		
		tasaSegunMarca = new HashMap<>();
		tasaSegunMarca.put("PERE", new PereTasaImpl());
		tasaSegunMarca.put("SCO", new ScoTasaImpl());
		tasaSegunMarca.put("SQUA", new SquaTasaImpl());
		
		Tasa tasa = tasaSegunMarca.get(marca);
		
		String resut  = "{\"Marca\":\""+marca+"\","
				+ "\"% Tasa\":\""+tasa.porcentajeAplicar()+"\","
				+ "\"Monto\":\""+monto+"\","
				+ "\"Valor tasa sobre monto\":\""+tasa.montoTasa(monto)+"\"}";
		
		return Response.status(200).entity(resut).build();
			         	              
}
}

    