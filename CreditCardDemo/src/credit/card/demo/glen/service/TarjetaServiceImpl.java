package credit.card.demo.glen.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import credit.card.demo.glen.domain.Tarjeta;
import credit.card.demo.glen.excepcion.HandledException;
import credit.card.demo.glen.excepcion.MontoActualizableException;
import credit.card.demo.glen.tasa.Tasa;

public class TarjetaServiceImpl implements TarjetaService{

	@Override
	public boolean realizarConsumo(Tarjeta tarjeta, double monto) throws Exception{
		// TODO Auto-generated method stub
		Tasa tasa  = tarjeta.getMarca().getTasa();
		double montoSegunTasa = tasa.montoTasa(monto)	;
		double cargoTotal = monto + montoSegunTasa;
		 
		if(verificarTarjeta(tarjeta)){
			if(monto <= 1000 && verificarSaldoDisponible(tarjeta, cargoTotal)){
				actualizarSaldo(tarjeta,cargoTotal);
				return true;
			}else{
				return false;
			}
			
			
		}else{
			return false;
		}
		
		
		
	}

	@Override
	public boolean verificarTarjeta(Tarjeta tarjeta) throws HandledException{
		// TODO Auto-generated method stub
		String fechaTarjeta = tarjeta.getFechaVto();
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha;
		try {
			fecha = formatoDeFecha.parse(fechaTarjeta);
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Calendar hoy = Calendar.getInstance();			
			
			if(hoy.getTime().before(fecha)){
				return true;	
			}else{
				return false;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new HandledException("No se puede verificar Tarjeta",e);
		}
		
		
		
	}

	@Override
	public boolean compararTarjeta(Tarjeta tarjeta, Tarjeta tarjeta2) throws Exception {
		// TODO Auto-generated method stub
		if(tarjeta.equals(tarjeta2)){
			return true;
		}else{
			return false;
		}
		
	}

	@Override
	public void actualizarSaldo(Tarjeta tarjeta,double cargoTotal) throws Exception{
		// TODO Auto-generated method stub
		try {
			double saldo = tarjeta.getMonto()- cargoTotal;
			tarjeta.setMonto(saldo);
		} catch (Exception e) {
			throw new MontoActualizableException("No se puede actualizar el monto",e);
		}
	}

	@Override
	public boolean verificarSaldoDisponible(Tarjeta tarjeta, double cargoTotal) throws Exception{
		// TODO Auto-generated method stub
		if (tarjeta.getMonto() >= cargoTotal){
			return true;
		}else{
			return false;
		}	
	}
	
	
		
}
