package credit.card.demo.glen.service;

import credit.card.demo.glen.domain.Tarjeta;

public interface TarjetaService {
	
	public boolean realizarConsumo(Tarjeta tarjeta, double monto) throws Exception;
	
	public boolean verificarTarjeta(Tarjeta tarjeta)throws Exception;
	
	public boolean compararTarjeta(Tarjeta tarjeta,Tarjeta tarjeta2)throws Exception;
	
	public void actualizarSaldo(Tarjeta tarjeta,double monto)throws Exception;
	
	public boolean verificarSaldoDisponible(Tarjeta tarjeta, double cargoTotal)throws Exception;
	
}
