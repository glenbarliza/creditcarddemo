package credit.card.demo.glen.tasa;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class SquaTasaImpl implements Tasa {

	@Override
	public float porcentajeAplicar() {
		// TODO Auto-generated method stub
		// a�o / mes		
		Calendar hoy = GregorianCalendar.getInstance();
	    return  (hoy.get(Calendar.YEAR)/hoy.get(Calendar.MONTH))/100;
	}
	@Override
	public double montoTasa(double monto) {
		// TODO Auto-generated method stub
		return monto * porcentajeAplicar();
	}

}
