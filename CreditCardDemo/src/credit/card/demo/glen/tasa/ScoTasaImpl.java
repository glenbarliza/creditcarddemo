package credit.card.demo.glen.tasa;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ScoTasaImpl implements Tasa {

	@Override
	public float porcentajeAplicar() {
		// TODO Auto-generated method stub
		// dia del mes *0.5
		Calendar hoy = GregorianCalendar.getInstance();
	    return  (float) (hoy.get(Calendar.DAY_OF_MONTH) * 0.5)/100;
	}
	
	@Override
	public double montoTasa(double monto) {
		// TODO Auto-generated method stub
		return monto * porcentajeAplicar();
	}

}
