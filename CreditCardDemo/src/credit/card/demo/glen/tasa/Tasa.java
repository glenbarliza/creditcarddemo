package credit.card.demo.glen.tasa;

public interface Tasa {
	
	public float porcentajeAplicar();
	
	public double montoTasa(double monto);
	
	
	
}
