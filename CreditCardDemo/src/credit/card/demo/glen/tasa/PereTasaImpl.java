package credit.card.demo.glen.tasa;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class PereTasaImpl implements Tasa {

	@Override
	public float porcentajeAplicar() {
		// TODO Auto-generated method stub
		// mes*0.1		
		Calendar hoy = GregorianCalendar.getInstance();
	    return  (float) (hoy.get(Calendar.MONTH) * 0.1)/100;	
	}

	@Override
	public double montoTasa(double monto) {
		// TODO Auto-generated method stub
		return monto * porcentajeAplicar();
	}

}
