package credit.card.demo.main;

import credit.card.demo.glen.domain.Marca;
import credit.card.demo.glen.domain.Tarjeta;
import credit.card.demo.glen.excepcion.ManejadorExcepciones;
import credit.card.demo.glen.service.TarjetaService;
import credit.card.demo.glen.service.TarjetaServiceImpl;
import credit.card.demo.glen.tasa.PereTasaImpl;
import credit.card.demo.glen.tasa.ScoTasaImpl;
import credit.card.demo.glen.tasa.SquaTasaImpl;
import credit.card.demo.glen.tasa.Tasa;

public class Ejecutar {
	
	public static void main(String[] args) {
	
	Tasa pereTasa = new PereTasaImpl();
	Tasa scoTasa = new ScoTasaImpl();
	Tasa squaTasa = new SquaTasaImpl();
		
	Marca pere = new Marca("PERE", pereTasa);
	Marca sco = new Marca("SCO", scoTasa);
	Marca squa = new Marca("SQUA",squaTasa);
	
	Tarjeta pereTarjeta = new Tarjeta(pere,"4546454645464546","Glen Barliza","30/06/2021",30000);
	Tarjeta scoTarjeta = new Tarjeta(sco,"5899589958995899","Glen Barliza","30/06/2021",25000);
	Tarjeta squaTarjeta = new Tarjeta(squa,"5012501250125012","Glen Barliza","30/06/2021",20000);
	
	//a) Invocar un m�todo que devuelva toda la informaci�n de una tarjeta
	System.out.println("Datos tarjeta de la marca PERE: "+pereTarjeta.toString());	
	System.out.println("Datos tarjeta de la marca SCO: "+scoTarjeta.toString());	
	System.out.println("Datos tarjeta de la marca SQUA: "+squaTarjeta.toString());
	
	TarjetaService tarjetaService = new TarjetaServiceImpl();
	
	/*b) Informar si una operaci�n es valida
		Una operaci�n es v�lida en el sistema si la persona 
		que opera en el mismo consume menos de 1000 pesos */
	
	 boolean result = false;
	try {
		result = tarjetaService.realizarConsumo(pereTarjeta, 900);
		if(result){
			 System.out.println("Operaci�n es v�lida");
			 System.out.println("Tarjeta marca: "+pereTarjeta.getMarca().getNombreMarca());
			 System.out.println("Valor tasa aplicar: "+pereTarjeta.getMarca().getTasa().porcentajeAplicar());
			 System.out.println("Monto operacion: "+900);
			 System.out.println("Costo operacion segun tasa: "+pereTarjeta.getMarca().getTasa().montoTasa(900));
			 System.out.println("Total operacion: "+ (900+pereTarjeta.getMarca().getTasa().montoTasa(900)));
		 }else{
			 System.out.println("Operaci�n no es v�lida");
		 }
	
	} catch (Exception e) {		
		ManejadorExcepciones.manejarException(e);
	}
	 
	}
	
	
}
